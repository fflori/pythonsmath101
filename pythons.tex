\documentclass[10pt,a4paper,english,twoside]{article}

% Using UTF-8: either use XeLaTeX/LuaLaTeX to compile, or use the inputenc package

\usepackage{hyperref}
\usepackage{pythons}
% \usepackage{showframe}                            % Show boxes
\usepackage{nameref}
\newcommand\gridWidth{\ensuremath{M}}
\newcommand\gridHeigth{\ensuremath{N}}
\newcommand\pointSize{\ensuremath{m}}

\title{Pythons}
\author{Francesco Florian}
\newcommand\assistantInfo{\href{mailto:francesco.florian@math.uzh.ch}{francesco.florian@math.uzh.ch}, office G25}

\begin{document}
\maketitle{}

\section{Objectives}
\label{sec:Objectives:pythons}
In this project you will create a version of the famous ``Snake'' video game\footnote{
  \url{https://en.wikipedia.org/wiki/Snake_(video_game_genre)}.}.

The player controls a python snake roaming on the screen and eating food (thus increasing its length and the player's score).
The game is lost as soon as the python crashes on an obstacle or on a part of its own body.

\begin{warning*}
  Read this document entirely \emph{before} you start coding!

  It is also strongly recommended that you approach the assistant in person before you start, and make sure that you contact him every time something is not clear.
\end{warning*}

Your program will be tested on a Linux computer with X, so you must ensure that any graphics library that you use supports X.
It also has to run on your computer (i.e., a computer where you have an account).

In general, compatibility with Windows and Unix-like systems using either X/Wayland (e.g., BSD and most Linux distributions) or Quartz (MacOs) is desirable and easy to achieve.
Compatibility with Android (and any other Linux distribution not using X) should be possible but may be nontrivial, so it is not required.
You may have to pay Apple to make your program run iOs, therefore this is also not required.

\section{Tasks}
\label{sec:Tasks:pythons}
The goal of this project is to experience programming in a group.
Discuss the project as a group and then divide the following tasks among yourselves.

Many of the guidelines in this section are not strict (some are just suggestions), if you prefer to do something in a different way, you can ask the assistant whether it is a possibility.

\subsection{Configuration file}
\label{sec:ConfigurationFile:pythons}
Some parameters of the game should be customizable:
\begin{itemize}
  \item these parameters should be stored in a configuration file, \texttt{settings.py}, in form of a Python script (but see the warning below);
  % \todo{optionally use json instead?}
  \item a command line option can be used to specify a different configuration file (you can use source file \texttt{pythons.py} as a starting point for this);
  \item in the following description, some variables will be marked as ``customizable'': you must include the support for reading at least these from the configuration file, but you can add others if you so wish.
\end{itemize}
Loading the configuration file should try to recover from errors as much as possible, e.g., your source code should contain defaults for every customizable variable, which will be used if that entry is missing in the settings file, or if the file cannot be read.
However, writing a properly formatted configuration file is the user's responsibility.

You must provide a sample configuration file that contains the default settings (with their description as a comment);
since the default settings will be used anyway if the file is not present, all the commands in the sample configuration should be commented as well.
\begin{example*}
  The file \texttt{settings.py} shows how the sample configuration for the variable \texttt{foo} may look.
\end{example*}
\begin{warning*}
  Letting a configuration file execute arbitrary code is in general a security issue!
  This is mostly apparent when the ``configuration file'' is some data that the user is supposed to load on a website\footnote{
    This issue has even a name (\url{https://en.wikipedia.org/wiki/Code\_injection}) when it is not implemented on purpose, but allowed by a software bug instead.}.

  This approach is appropriate here because it is reasonable to assume that whoever writes the configuration file is also the user who will execute the program:
  in this case, writing malicious code into the configuration file is not easier than directly typing it on the command line.
\end{warning*}

\subsection{Icons}
\label{sec:Icons:pythons}
The python (snake), the walls, the food and possibly other items are represented on the screen by square icons of size \pointSize{}px.
When your program starts, it must load all the required icons, from a customizable location (alternatively, the customization file may include a list of files to load, one for each needed icon).

The dimension \pointSize{} can be a fixed constant (or equivalently a customizable setting), or given implicitly via the icon size;
in the former case your code \emph{must} resize the icons, if the dimensions are wrong;
in the latter, your code \emph{must} ensure that all the loaded icons are square and have the same size, and fall back the other option if this is not the case.

Several libraries that let you work with icons and write GUI applications exist (\texttt{PySide} is recommended):
\begin{itemize}
  \item \texttt{PyQt}\footnote{
    Documentation: \url{https://www.riverbankcomputing.com/static/Docs/PyQt5/}.}, or (better) \texttt{PySide}\footnote{
    Documentation: \url{https://doc.qt.io/qtforpython/api.html}. Homepage: \url{https://wiki.qt.io/PySide2}.} are two similar libraries providing a way to use the powerful Qt library from Python;
  you can load your icons using a \texttt{QPixmap}\footnote{
    \url{https://doc.qt.io/qtforpython/PySide2/QtGui/QPixmap.html}.}, convert them to \texttt{QGraphicsPixmapItem}\footnote{
    \url{https://doc.qt.io/qtforpython/PySide2/QtWidgets/QGraphicsPixmapItem.html}.} and draw them in a \texttt{QGraphicsView} widget\footnote{
    Useful documentation:\\
    \url{https://doc.qt.io/qtforpython/PySide2/QtWidgets/QGraphicsView.html},\\
    \url{https://doc.qt.io/qtforpython/PySide2/QtWidgets/QGraphicsScene.html} and\\
    \url{https://doc.qt.io/qtforpython/PySide2/QtWidgets/QGraphicsScene.html\#PySide2.QtWidgets.PySide2.QtWidgets.QGraphicsScene.addItem}.}.
  The sample file \texttt{pythons.py} contains a few example commands showing how to use \texttt{PySide2}.
  
  Advantages: full access to the Qt library, which has plenty of well documented features;
  \nameref{sec:ManagingTheInput:pythons} and \nameref{sec:MusicAndSounds:pythons} are also possible with Qt.
  Furthermore, designing a menu and message/confirmation dialogs is very easy (if you wish to use any).
  Disadvantages: the learning curve is a bit steeper than that of most other tools in the beginning (but the assistant will help with that).
  \item \texttt{tkinter}\footnote{
    Documentation: \url{https://docs.python.org/3/library/tkinter.html}.}.
  Advantages: it is the only GUI library which is built-in in a standard Python installation;
  Disadvantages: this library is somewhat limited (you may not find a widget doing what you need), has a poor documentation (the assistant may be less able to help), and many functions modify hidden global variables, making it somewhat difficult to use.
  If you decide to use \texttt{tkinter}, you can use pillow\footnote{
    Documentation: \url{https://pillow.readthedocs.io}.} for loading the icons;
  you can then print them on a \texttt{tkinter} \texttt{Canvas} widget (from python version 3.8);
  a \texttt{Label} widget might work for earlier versions.
  \item The page \url{https://wiki.python.org/moin/GuiProgramming} contains a list of alternatives;
  you can look there if you don't like the two suggestions above (but ask if the assistant agrees with your choice before starting using it).
\end{itemize}

\begin{warning*}
  Before using an icon file that you found in the internet (or any source), check that the license allows you to embed it in your program! (If the page you found it has a ``all rights reserved'' notice or there is no license at all it probably does not allow you to use it in your project.)

  If you violate the copyright, this will not prevent you to pass the course, but the copyright holder might decide to sue you if they find out.

  Designing your own icons (e.g., a uniformly colored square) is a good way to avoid problems if you don't want to search (since the icons are customizable, your users will not be stuck with those).

  If you decide to license your program with something compatible with the GPLv3, using GIMP brushes can also be a suitable alternative.
\end{warning*}

\begin{hint*}
  You should make sure that your icons are saved in some standard format (e.g., PNG, SVG or JPEG) to avoid compatibility issues when you try to use your program on a different system.
\end{hint*}

\subsection{Walls}
\label{sec:Walls:pythons}
The wall may be placed at any point in the grid.
They should have a (customizable) icon.
The location of the walls may be fixed, or may be described in the settings file, or in an additional file whose location is written in the settings file.

\subsection{The board}
\label{sec:TheBoard:pythons}
The game board is a $\gridHeigth \times \gridWidth$ grid, (both \gridHeigth{} and \gridWidth{} are customizable).
Each point in the gird may, at any time, contain an object.

``Printing'' the board means drawing a background of proper size in a window, and any object that is placed in the grid points.
Each grid point must ``occupy'' a square space corresponding to an icon.

\begin{hint*}
  You can represent the grid as a list, where the empty points contain the value \verb+None+.
  The other points can either contain a (reference to) the object positioned there, or two functions, one which draws the proper icon at the grid place, and one that is executed whenever the snake enters that point\footnote{
  This is often named a ``callback'' function.} (or a mix of these two approaches).
\end{hint*}

\subsection{The snake}
\label{sec:TheSnake:pythons}
Your python has a head, a body, and a tail, which are represented by different icons.
Moreover, some icons may require to be rotated, depending on the direction where the python is heading.
Besides, the points where the body turns, may need to be drawn using a different icon (otherwise they might look ugly
).

Since the icons you use are customizable, you must always rotate them when appropriate and check where the turns are, even if for some icon sets these operations may be irrelevant.

The variable describing the snake should keep track of all the gridpoints that it is on, and any function that moves the snake should also take care of updating the grid when needed.

It also should contain a counter describing how much food is still to process (i.e., how much your python still has to grow due to the food that it has already eaten).

\subsection{Managing the input}
\label{sec:ManagingTheInput:pythons}
You need to move the snake according to the user input.
The most common way to do so is to check if the user is pressing any key on the keyboard.

The recommended way is to use keyboard events from \texttt{Qt}\footnote{
  Relevant class: \url{https://doc.qt.io/qtforpython/PySide2/QtGui/QKeyEvent.html}.}.

You can also use \texttt{tkinter} (poorly documented) or \texttt{keyboard.Listener} from pynput\footnote{
  Relevant documentation: \url{https://pynput.readthedocs.io/en/latest/keyboard.html\#monitoring-the-keyboard}.
  Also beware: \url{https://pynput.readthedocs.io/en/latest/limitations.html}.}.

\subsection{Advancing the time}
\label{sec:AdvancingTheTime:pythons}
The game proceeds in discrete time steps.
At each step you have to:
\begin{itemize}
  \item Move the python's tail, if necessary, otherwise decrease the food processing counter described in \nameref{sec:TheSnake:pythons}.
  \item Check for user input and update the movement direction.
  \item Compute the next position of the python's head.
  \item Check for collisions: if the head of the python will be on a grid point that already contains an item, you should perform the appropriate action, depending on the item it is on:
  \begin{itemize}
    \item for a food item, increase the food processing counter described in \nameref{sec:TheSnake:pythons}, update the score and place a new food item;
    \item for a wall, or a python's body part, exit the game stating that the player has lost, and its score;
    \item for another item, perform the appropriate action.
  \end{itemize}
  \item Place any further item, if appropriate.
\end{itemize}

\subsection{The program}
\label{sec:TheProgram:pythons}
Your main function should load the settings, create the board window, position the python and an initial food item, then \hyperref[sec:TheBoard:pythons]{print the board}.
As soon as any key (or a specific set of keys) is pressed, the python should start moving, and a loop \nameref{sec:AdvancingTheTime:pythons} should start.

From the code point of view, the ``movement'' is a periodic update of the position of the python and the other grid items (if appropriate), as described in \nameref{sec:AdvancingTheTime:pythons}:
after you perform all the needed actions, you should pause until the next time step.

A nice way of doing this is using timers\footnote{
  E.g., using \texttt{QTimer}: \url{https://doc.qt.io/qtforpython/PySide2/QtCore/QTimer.html}, or \url{https://docs.python.org/3/library/threading.html\#timer-objects}.}; the function \texttt{sleep}\footnote{
  See \url{https://docs.python.org/3/library/time.html\#time.sleep}.} can also be used.

The sleep or timer interval is customizable (a finite number of ``difficulty levels'' corresponding to various intervals can be provided instead).

\subsection{Bonus points}
\label{sec:BonusPoints:pythons}
These tasks describe some additional features that can make your program more interesting, but are not mandatory (the don't contribute to the points for the project, so you get the full points even if they are not implemented).

Concentrate on writing readable code, and on making all of the main features work before you implement any of the optional features!

You are free to implement all of the optional features, some of them or even none of them, or to implement additional feature different from those described here.

\subsubsection{Bonus points}
\label{sec:BonusPointsSub:pythons}
Besides the food, a different kind of item can randomly appear on the screen (and stay there only for a finite amount of time);
eating these items will increase the score without making your python grow (or making it grow less than the ``normal'' food, or they can make it shrink without increasing the points\dots{})

\subsubsection{Customization menu}
\label{sec:CustomizationMenu:pythons}
You can offer a customization menu that changes the game parameters and optionally also writes the configuration file.

\subsubsection{Music and sounds}
\label{sec:MusicAndSounds:pythons}
You can have your program play a background music, and optionally a different sound for each kind of event\footnote{
  \texttt{Qt} also supports multimedia and audio: \url{https://doc.qt.io/qtforpython/PySide2/QtMultimedia/QAudioOutput.html}.} (game lost, food eaten, bonus eaten, \dots{})

Note that, like images, most audio tracks are copyrighted, and you cannot freely add them to your program.

\subsubsection{Ester egg}
\label{sec:EsterEgg:pythons}
You can include an Easter egg in your game\footnote{
  \url{https://en.wikipedia.org/wiki/Easter\_egg\_(media)}}, i.e., an hidden feature.

\subsubsection{levels}
\label{sec:levels:pythons}
You can offer different levels, based on different walls positions.

\subsubsection{Multiplayer}
\label{sec:Multiplayer:pythons}
You can offer a multiplayer mode, where two pythons are present.
In this case you have to decide which are the rules for determining the winner.

\section{Additional guidelines}
\label{sec:Notes:pythons}

\subsection{Coding style}
\label{sec:CodingStyle:pythons}
Decide a coding style and use it consistently across the project.
Part of the points reserved for the project will only be awarded if the code is readable and a consistent coding style is used.
This means that you \emph{must}:
\begin{itemize}
  \item Comment your code;
  it is particularly important that you document your functions, and any parts of the code whose purpose is not obvious.
  \item Use descriptive variable names;
  some of the names in the tasks description purposely violate this requirement: change them.
  \item Functions should only access and modify their parameters and local variables, not variables defined outside the function;
  this means that you should avoid global variables;
  exceptions:
  \begin{itemize}
    \item program-wide hardcoded constants (i.e., read only variables) may be global;
    \item variables loaded from the settings file may be global as well, since they represent global configuration parameters that behave as constants (you only initialize them when starting the program and never change them afterwards);
    \item \nameref{sec:ManagingTheInput:pythons} can update a global variable;
    \item some other (rare) uses might be acceptable.
  \end{itemize}
  \item Use functions whenever it is appropriate, do \emph{not} write only one function per task.
  \item Optional, but recommended: your functions should not print on the screen, or draw items, and access or modify only function parameters and local variables;
  exceptions:
  \begin{itemize}
    \item if the only purpose of that function is to print or draw something, e.g., \hyperref[sec:TheBoard:pythons]{printing the board} should draw on the screen, but not access global variables;
    \item \nameref{sec:ManagingTheInput:pythons} is allowed to read and modify global variables;
    those variables will then be accessed somewhere else.
    \item some other (rare) uses might be acceptable.
  \end{itemize}
\end{itemize}

Your settings file is also a Python script: as such, it must also be properly written, and commented, as described above.
The only difference is that its sole purpose is to set some parameters that you probably treat as global variables, and it will probably mostly consist of assignment like \texttt{variable=constant} instead of functions.

\subsection{Functions and data structures}
\label{sec:FunctionsAndDataStructures:pythons}
Since you must avoid global variables as much as possible, some of your variables may need to be a collection of other variables.

In most cases you can implement these as lists, but sometimes dictionaries can be more convenient.
If you feel that you prefer classes, you can use them (that can especially handy if you want to implement some tricks with Qt), but it is possible to complete this project not using them at all (except for those you import from libraries).

In any case, be sure that you define the kind of variables that you will use;
since different persons in the group will implement different functions, be sure that you decide function type signatures (i.e., what the parameters and the return value will be) \emph{before} you start implementing or referencing them, since changing these things afterwards can be quite painful.

\subsection{Project handout}
\label{sec:LaTeX:pythons}
Using \LaTeX{} for the description and the presentation is recommended (but not required).

It is usually a bad idea\footnote{
  This is actually a personal preference of the assistant, you are free to ignore it; however any code you include will not count for measuring the length of the handout.} to include any source code in the handout (and even worse for the presentation);
if you think that showing your approach or algorithm is worth, include that, not the code.

This allows the audience to understand the main ideas, without the need of go through all the technical details (i.e., it saves your audience both time and efforts);
if ever someone will also need the details, you can make the code available separately.

\subsection{Git}
\label{sec:Git:pythons}
Using git is recommended (but not required)\footnote{
  You can also get the project template files and this document source from the repository \url{git.math.uzh.ch:fflori:fflori/pythonsmath101.git}.}:
create your git repository, either on the math institute servers or on any place that you like;
you are then advised to give the assistant access to the repository, so that he can check your progress and have a quicker way to access your code in case you need help on the code.

It also makes it easier to publish your code when it's finished (if you wish to do so).

Ask the assistant for help if you would like to use git but you don't know how to get started.

Note that git is useful for tracking both Python and \TeX{} code!
You can therefore submit your project by simply stating that it is ready on git, instead of sending everything by email.

\input{notes}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
