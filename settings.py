## Settings file controlling the behavior of the "Pythons" game

# The foo variable controls the fooness of the snake tail.
# Its possible values are: the absolute path to an audio file
# in 'ogg' format, which will be played whenever the snake fooes,
# or the strings 'bar' 'baz' 'barbaz', which instruct the
# program to play builtin default sounds.
# Uncomment the following line to customize the variable.
# foo = 'bar'

# sample assignment to show how to import variables in tha main program
bloh = "bloh"
