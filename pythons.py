#!/usr/bin/python3
import importlib
import argparse
import sys
import PySide2
from PySide2 import QtWidgets
from PySide2 import QtGui

#set the default settings file as "settings.py"
defaultSettingsFile = "settings"

# Parse the command line to get an alternate settings file
# Import the file given on the command line, fall back to the default in case of failure
# @return the imported module
def parseSettings():
    parser = argparse.ArgumentParser(description="A game about you controlling a python.")
    parser.add_argument("--config", metavar="<file>", nargs="?", help="Use <file>.py as the configuration file.", dest = "settingsFile")
    parserArgs = parser.parse_args()
    if parserArgs.settingsFile is not None:
        try:
            settings = importlib.import_module(parserArgs.settingsFile)
        except ModuleNotFoundError:
            settings = importlib.import_module(defaultSettingsFile)
    else:
        settings = importlib.import_module(defaultSettingsFile)

    return settings


if __name__ == "__main__":
    # Import the settings module
    settings = parseSettings()
    print(settings.bloh)
    
    # Create a QApplication before starting to work with the Qt library
    app = QtWidgets.QApplication(sys.argv)

    # Open the icon file, then create two copy of the "apple" icon
    appleIcon = QtGui.QPixmap("apple.png")
    apple = QtWidgets.QGraphicsPixmapItem(appleIcon)
    apple2 = QtWidgets.QGraphicsPixmapItem(appleIcon)
    apple2.setOffset(10,10)     #this item will be positioned at (10,10)

    # Create a scene, and a view that displays the scene
    scene = QtWidgets.QGraphicsScene()
    view = QtWidgets.QGraphicsView(scene)

    # Draw the apples on the scene
    scene.addItem(apple)
    scene.addItem(apple2)

    # Show the window, and start processing the event (e.g., the close button)
    view.show()
    sys.exit(app.exec_())
